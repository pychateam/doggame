// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Mydoggo/MydoggoHUD.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMydoggoHUD() {}
// Cross Module References
	MYDOGGO_API UClass* Z_Construct_UClass_AMydoggoHUD_NoRegister();
	MYDOGGO_API UClass* Z_Construct_UClass_AMydoggoHUD();
	ENGINE_API UClass* Z_Construct_UClass_AHUD();
	UPackage* Z_Construct_UPackage__Script_Mydoggo();
// End Cross Module References
	void AMydoggoHUD::StaticRegisterNativesAMydoggoHUD()
	{
	}
	UClass* Z_Construct_UClass_AMydoggoHUD_NoRegister()
	{
		return AMydoggoHUD::StaticClass();
	}
	struct Z_Construct_UClass_AMydoggoHUD_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AMydoggoHUD_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AHUD,
		(UObject* (*)())Z_Construct_UPackage__Script_Mydoggo,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMydoggoHUD_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Rendering Actor Input Replication" },
		{ "IncludePath", "MydoggoHUD.h" },
		{ "ModuleRelativePath", "MydoggoHUD.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_AMydoggoHUD_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AMydoggoHUD>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AMydoggoHUD_Statics::ClassParams = {
		&AMydoggoHUD::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x008002ACu,
		METADATA_PARAMS(Z_Construct_UClass_AMydoggoHUD_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AMydoggoHUD_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AMydoggoHUD()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AMydoggoHUD_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AMydoggoHUD, 300092789);
	template<> MYDOGGO_API UClass* StaticClass<AMydoggoHUD>()
	{
		return AMydoggoHUD::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AMydoggoHUD(Z_Construct_UClass_AMydoggoHUD, &AMydoggoHUD::StaticClass, TEXT("/Script/Mydoggo"), TEXT("AMydoggoHUD"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AMydoggoHUD);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
