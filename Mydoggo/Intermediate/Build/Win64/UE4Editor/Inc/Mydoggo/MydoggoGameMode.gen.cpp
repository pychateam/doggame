// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Mydoggo/MydoggoGameMode.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMydoggoGameMode() {}
// Cross Module References
	MYDOGGO_API UClass* Z_Construct_UClass_AMydoggoGameMode_NoRegister();
	MYDOGGO_API UClass* Z_Construct_UClass_AMydoggoGameMode();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_Mydoggo();
// End Cross Module References
	void AMydoggoGameMode::StaticRegisterNativesAMydoggoGameMode()
	{
	}
	UClass* Z_Construct_UClass_AMydoggoGameMode_NoRegister()
	{
		return AMydoggoGameMode::StaticClass();
	}
	struct Z_Construct_UClass_AMydoggoGameMode_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AMydoggoGameMode_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_Mydoggo,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMydoggoGameMode_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "MydoggoGameMode.h" },
		{ "ModuleRelativePath", "MydoggoGameMode.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_AMydoggoGameMode_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AMydoggoGameMode>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AMydoggoGameMode_Statics::ClassParams = {
		&AMydoggoGameMode::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x008802ACu,
		METADATA_PARAMS(Z_Construct_UClass_AMydoggoGameMode_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AMydoggoGameMode_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AMydoggoGameMode()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AMydoggoGameMode_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AMydoggoGameMode, 4276151068);
	template<> MYDOGGO_API UClass* StaticClass<AMydoggoGameMode>()
	{
		return AMydoggoGameMode::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AMydoggoGameMode(Z_Construct_UClass_AMydoggoGameMode, &AMydoggoGameMode::StaticClass, TEXT("/Script/Mydoggo"), TEXT("AMydoggoGameMode"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AMydoggoGameMode);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
