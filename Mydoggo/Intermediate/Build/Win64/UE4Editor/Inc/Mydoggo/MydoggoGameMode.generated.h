// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MYDOGGO_MydoggoGameMode_generated_h
#error "MydoggoGameMode.generated.h already included, missing '#pragma once' in MydoggoGameMode.h"
#endif
#define MYDOGGO_MydoggoGameMode_generated_h

#define Mydoggo_Source_Mydoggo_MydoggoGameMode_h_12_SPARSE_DATA
#define Mydoggo_Source_Mydoggo_MydoggoGameMode_h_12_RPC_WRAPPERS
#define Mydoggo_Source_Mydoggo_MydoggoGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define Mydoggo_Source_Mydoggo_MydoggoGameMode_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAMydoggoGameMode(); \
	friend struct Z_Construct_UClass_AMydoggoGameMode_Statics; \
public: \
	DECLARE_CLASS(AMydoggoGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Mydoggo"), MYDOGGO_API) \
	DECLARE_SERIALIZER(AMydoggoGameMode)


#define Mydoggo_Source_Mydoggo_MydoggoGameMode_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAMydoggoGameMode(); \
	friend struct Z_Construct_UClass_AMydoggoGameMode_Statics; \
public: \
	DECLARE_CLASS(AMydoggoGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Mydoggo"), MYDOGGO_API) \
	DECLARE_SERIALIZER(AMydoggoGameMode)


#define Mydoggo_Source_Mydoggo_MydoggoGameMode_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	MYDOGGO_API AMydoggoGameMode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMydoggoGameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(MYDOGGO_API, AMydoggoGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMydoggoGameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	MYDOGGO_API AMydoggoGameMode(AMydoggoGameMode&&); \
	MYDOGGO_API AMydoggoGameMode(const AMydoggoGameMode&); \
public:


#define Mydoggo_Source_Mydoggo_MydoggoGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	MYDOGGO_API AMydoggoGameMode(AMydoggoGameMode&&); \
	MYDOGGO_API AMydoggoGameMode(const AMydoggoGameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(MYDOGGO_API, AMydoggoGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMydoggoGameMode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AMydoggoGameMode)


#define Mydoggo_Source_Mydoggo_MydoggoGameMode_h_12_PRIVATE_PROPERTY_OFFSET
#define Mydoggo_Source_Mydoggo_MydoggoGameMode_h_9_PROLOG
#define Mydoggo_Source_Mydoggo_MydoggoGameMode_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Mydoggo_Source_Mydoggo_MydoggoGameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	Mydoggo_Source_Mydoggo_MydoggoGameMode_h_12_SPARSE_DATA \
	Mydoggo_Source_Mydoggo_MydoggoGameMode_h_12_RPC_WRAPPERS \
	Mydoggo_Source_Mydoggo_MydoggoGameMode_h_12_INCLASS \
	Mydoggo_Source_Mydoggo_MydoggoGameMode_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Mydoggo_Source_Mydoggo_MydoggoGameMode_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Mydoggo_Source_Mydoggo_MydoggoGameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	Mydoggo_Source_Mydoggo_MydoggoGameMode_h_12_SPARSE_DATA \
	Mydoggo_Source_Mydoggo_MydoggoGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	Mydoggo_Source_Mydoggo_MydoggoGameMode_h_12_INCLASS_NO_PURE_DECLS \
	Mydoggo_Source_Mydoggo_MydoggoGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MYDOGGO_API UClass* StaticClass<class AMydoggoGameMode>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Mydoggo_Source_Mydoggo_MydoggoGameMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
