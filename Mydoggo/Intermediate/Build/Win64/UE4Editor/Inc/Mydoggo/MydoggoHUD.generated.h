// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MYDOGGO_MydoggoHUD_generated_h
#error "MydoggoHUD.generated.h already included, missing '#pragma once' in MydoggoHUD.h"
#endif
#define MYDOGGO_MydoggoHUD_generated_h

#define Mydoggo_Source_Mydoggo_MydoggoHUD_h_12_SPARSE_DATA
#define Mydoggo_Source_Mydoggo_MydoggoHUD_h_12_RPC_WRAPPERS
#define Mydoggo_Source_Mydoggo_MydoggoHUD_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define Mydoggo_Source_Mydoggo_MydoggoHUD_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAMydoggoHUD(); \
	friend struct Z_Construct_UClass_AMydoggoHUD_Statics; \
public: \
	DECLARE_CLASS(AMydoggoHUD, AHUD, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Mydoggo"), NO_API) \
	DECLARE_SERIALIZER(AMydoggoHUD)


#define Mydoggo_Source_Mydoggo_MydoggoHUD_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAMydoggoHUD(); \
	friend struct Z_Construct_UClass_AMydoggoHUD_Statics; \
public: \
	DECLARE_CLASS(AMydoggoHUD, AHUD, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Mydoggo"), NO_API) \
	DECLARE_SERIALIZER(AMydoggoHUD)


#define Mydoggo_Source_Mydoggo_MydoggoHUD_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMydoggoHUD(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMydoggoHUD) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMydoggoHUD); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMydoggoHUD); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMydoggoHUD(AMydoggoHUD&&); \
	NO_API AMydoggoHUD(const AMydoggoHUD&); \
public:


#define Mydoggo_Source_Mydoggo_MydoggoHUD_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMydoggoHUD(AMydoggoHUD&&); \
	NO_API AMydoggoHUD(const AMydoggoHUD&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMydoggoHUD); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMydoggoHUD); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AMydoggoHUD)


#define Mydoggo_Source_Mydoggo_MydoggoHUD_h_12_PRIVATE_PROPERTY_OFFSET
#define Mydoggo_Source_Mydoggo_MydoggoHUD_h_9_PROLOG
#define Mydoggo_Source_Mydoggo_MydoggoHUD_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Mydoggo_Source_Mydoggo_MydoggoHUD_h_12_PRIVATE_PROPERTY_OFFSET \
	Mydoggo_Source_Mydoggo_MydoggoHUD_h_12_SPARSE_DATA \
	Mydoggo_Source_Mydoggo_MydoggoHUD_h_12_RPC_WRAPPERS \
	Mydoggo_Source_Mydoggo_MydoggoHUD_h_12_INCLASS \
	Mydoggo_Source_Mydoggo_MydoggoHUD_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Mydoggo_Source_Mydoggo_MydoggoHUD_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Mydoggo_Source_Mydoggo_MydoggoHUD_h_12_PRIVATE_PROPERTY_OFFSET \
	Mydoggo_Source_Mydoggo_MydoggoHUD_h_12_SPARSE_DATA \
	Mydoggo_Source_Mydoggo_MydoggoHUD_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	Mydoggo_Source_Mydoggo_MydoggoHUD_h_12_INCLASS_NO_PURE_DECLS \
	Mydoggo_Source_Mydoggo_MydoggoHUD_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MYDOGGO_API UClass* StaticClass<class AMydoggoHUD>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Mydoggo_Source_Mydoggo_MydoggoHUD_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
