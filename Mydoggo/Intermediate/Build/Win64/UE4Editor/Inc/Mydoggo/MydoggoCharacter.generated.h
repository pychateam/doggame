// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MYDOGGO_MydoggoCharacter_generated_h
#error "MydoggoCharacter.generated.h already included, missing '#pragma once' in MydoggoCharacter.h"
#endif
#define MYDOGGO_MydoggoCharacter_generated_h

#define Mydoggo_Source_Mydoggo_MydoggoCharacter_h_14_SPARSE_DATA
#define Mydoggo_Source_Mydoggo_MydoggoCharacter_h_14_RPC_WRAPPERS
#define Mydoggo_Source_Mydoggo_MydoggoCharacter_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define Mydoggo_Source_Mydoggo_MydoggoCharacter_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAMydoggoCharacter(); \
	friend struct Z_Construct_UClass_AMydoggoCharacter_Statics; \
public: \
	DECLARE_CLASS(AMydoggoCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Mydoggo"), NO_API) \
	DECLARE_SERIALIZER(AMydoggoCharacter)


#define Mydoggo_Source_Mydoggo_MydoggoCharacter_h_14_INCLASS \
private: \
	static void StaticRegisterNativesAMydoggoCharacter(); \
	friend struct Z_Construct_UClass_AMydoggoCharacter_Statics; \
public: \
	DECLARE_CLASS(AMydoggoCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Mydoggo"), NO_API) \
	DECLARE_SERIALIZER(AMydoggoCharacter)


#define Mydoggo_Source_Mydoggo_MydoggoCharacter_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMydoggoCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMydoggoCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMydoggoCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMydoggoCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMydoggoCharacter(AMydoggoCharacter&&); \
	NO_API AMydoggoCharacter(const AMydoggoCharacter&); \
public:


#define Mydoggo_Source_Mydoggo_MydoggoCharacter_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMydoggoCharacter(AMydoggoCharacter&&); \
	NO_API AMydoggoCharacter(const AMydoggoCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMydoggoCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMydoggoCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AMydoggoCharacter)


#define Mydoggo_Source_Mydoggo_MydoggoCharacter_h_14_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Mesh1P() { return STRUCT_OFFSET(AMydoggoCharacter, Mesh1P); } \
	FORCEINLINE static uint32 __PPO__FP_Gun() { return STRUCT_OFFSET(AMydoggoCharacter, FP_Gun); } \
	FORCEINLINE static uint32 __PPO__FP_MuzzleLocation() { return STRUCT_OFFSET(AMydoggoCharacter, FP_MuzzleLocation); } \
	FORCEINLINE static uint32 __PPO__VR_Gun() { return STRUCT_OFFSET(AMydoggoCharacter, VR_Gun); } \
	FORCEINLINE static uint32 __PPO__VR_MuzzleLocation() { return STRUCT_OFFSET(AMydoggoCharacter, VR_MuzzleLocation); } \
	FORCEINLINE static uint32 __PPO__FirstPersonCameraComponent() { return STRUCT_OFFSET(AMydoggoCharacter, FirstPersonCameraComponent); } \
	FORCEINLINE static uint32 __PPO__R_MotionController() { return STRUCT_OFFSET(AMydoggoCharacter, R_MotionController); } \
	FORCEINLINE static uint32 __PPO__L_MotionController() { return STRUCT_OFFSET(AMydoggoCharacter, L_MotionController); }


#define Mydoggo_Source_Mydoggo_MydoggoCharacter_h_11_PROLOG
#define Mydoggo_Source_Mydoggo_MydoggoCharacter_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Mydoggo_Source_Mydoggo_MydoggoCharacter_h_14_PRIVATE_PROPERTY_OFFSET \
	Mydoggo_Source_Mydoggo_MydoggoCharacter_h_14_SPARSE_DATA \
	Mydoggo_Source_Mydoggo_MydoggoCharacter_h_14_RPC_WRAPPERS \
	Mydoggo_Source_Mydoggo_MydoggoCharacter_h_14_INCLASS \
	Mydoggo_Source_Mydoggo_MydoggoCharacter_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Mydoggo_Source_Mydoggo_MydoggoCharacter_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Mydoggo_Source_Mydoggo_MydoggoCharacter_h_14_PRIVATE_PROPERTY_OFFSET \
	Mydoggo_Source_Mydoggo_MydoggoCharacter_h_14_SPARSE_DATA \
	Mydoggo_Source_Mydoggo_MydoggoCharacter_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	Mydoggo_Source_Mydoggo_MydoggoCharacter_h_14_INCLASS_NO_PURE_DECLS \
	Mydoggo_Source_Mydoggo_MydoggoCharacter_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MYDOGGO_API UClass* StaticClass<class AMydoggoCharacter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Mydoggo_Source_Mydoggo_MydoggoCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
